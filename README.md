External Script Runner for Confluence
--------------------------------------

#### This is just a sample add-on, and commercial usage is not recommended.

  * This add-on create a macro that has a button to fire python script in `resources` directory.
  * The output of script (i.e. txt file here) is attached to page from where macro code is fired.
  * For more details ask me on Atlassian Community - [My Profile](https://community.atlassian.com/t5/user/viewprofilepage/user-id/690016)


Here are the SDK commands you'll use immediately:

  * `atlas-run`   -- installs this plugin into the product and starts it on localhost
  * `atlas-debug` -- same as atlas-run, but allows a debugger to attach at port 5005
  * `atlas-help`  -- prints description for all commands in the SDK
