//This file will call API and alert user if successfully we have added a attachment to provided page
AJS.toInit(function($) {

  //Assining click event to button that we have added via macro
  $(".external-script-caller").click(function(eve) {
    var self = this;
    eve.preventDefault();

    //Showing spinner in button
    self.setAttribute("aria-disabled", "true");
    $(self).siblings('.button-spinner').spin();

    //Get page-id from local js meta
    var page = AJS.params.pageId;

    // as confluence allow you to add macro at various places other then page
    if (page) {
      //here contextPath is variable provided by confluence
      //path to rest api is mix of module defination in atlassian-plugin.xml and rest resource defined in java
      $.getJSON(contextPath + "/rest/esr/1.0/script/run?page=" + page, function(data) {
        self.setAttribute("aria-disabled", "false");
        $(self).siblings('.button-spinner').spinStop();

        //Not checking for errors etc.
        if (data.status === true) {
          AJS.flag({
            type: 'info',
            title: "Script successfully executed",
            body:
              "<p>Attachment named " +
                "<strong>" + data.attachment_name + "</strong>" +
              " added to current page.</p>"
          });
        } else {
          AJS.flag({
            type: 'error',
            title: data.error ? data.error : "Something went wrong",
            body:
              "<dl>" +
                "<dt>Script Code</dt>" +
                "<dd>" + data.script_exit_code + "</dd>" +
                "<dt>Script Error</dt>" +
                "<dd>" + data.script_error + "</dd>" +
              "</dl>"
          });
        }
      });
    } else {
      AJS.flag({
        type: 'warning',
        title: "Invalid page",
        body: "<p>This is not a valid confluence page<p>"
      });

      self.setAttribute("aria-disabled", "false");
      $(self).siblings('.button-spinner').spinStop();
    }

    return false;
  });
});
