# This file will write a txt in give location with some params
# Not handling much of errors here
import sys, getopt

def main(argv):
  input_text = ''
  file_location = ''

  try:
    opts, args = getopt.getopt(argv, "hi:f:", ["itxt=", "flocation="])
  except getopt.GetoptError:
    print('script.py -i <input_text> -f <file_location>')
    sys.exit(2)
  
  for opt, arg in opts:
    if opt == '-h':
      print('script.py -i <input_text> -f <file_location>')
      sys.exit()
    elif opt in ("-i", "--itxt"):
      input_text = arg
    elif opt in ("-f", "--flocation"):
      file_location = arg

  file = open(file_location, "w+")
  
  #Repeating it 100 times just to make some sense
  for i in range(100):
    file.write("Line number: %s - %s \n" % (i, input_text))

  file.close()

if __name__ == "__main__":
  main(sys.argv[1:])
