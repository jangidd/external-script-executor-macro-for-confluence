package com.example.confluence.plugins.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "script")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScriptRestResourceModel
{

  @XmlElement(name = "status")
  private boolean status;

  @XmlElement(name = "attachment_name")
  private String attachment;

  @XmlElement(name = "error")
  private String error;

  @XmlElement(name = "script_exit_code")
  private int scriptExecutionCode;

  @XmlElement(name = "script_output")
  private String scriptOutput;

  @XmlElement(name = "script_error")
  private String scriptError;

  public ScriptRestResourceModel()
  {
  }

  public boolean getStatus()
  {
    return status;
  }

  public void setStatus(boolean status)
  {
    this.status = status;
  }

  public String getError()
  {
    return error;
  }

  public void setError(String error)
  {
    this.error = error;
  }

  public String getAttachment()
  {
    return attachment;
  }

  public void setAttachment(String attachment)
  {
    this.attachment = attachment;
  }

  public int getScriptExecutionCode()
  {
    return scriptExecutionCode;
  }

  public void setScriptExecutionCode(int scriptExecutionCode)
  {
    this.scriptExecutionCode = scriptExecutionCode;
  }

  public String getScriptOutput()
  {
    return scriptOutput;
  }

  public void setScriptOutput(String scriptOutput)
  {
    this.scriptOutput = scriptOutput;
  }

  public String getScriptError()
  {
    return scriptError;
  }

  public void setScriptError(String scriptError)
  {
    this.scriptError = scriptError;
  }
}