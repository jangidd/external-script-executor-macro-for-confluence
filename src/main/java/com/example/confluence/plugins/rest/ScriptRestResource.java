package com.example.confluence.plugins.rest;

import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.example.confluence.plugins.utils.ExternalScriptRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

/**
 * A resource of message.
 */
@Path("/script")
@Scanned
public class ScriptRestResource
{
  private static final Logger log = LoggerFactory.getLogger(ScriptRestResource.class);

  @ComponentImport
  private PageManager pageManager;

  @ComponentImport
  private AttachmentManager attachmentManager;


  @Inject
  public ScriptRestResource(PageManager pageManager, AttachmentManager attachmentManager)
  {
    this.pageManager = pageManager;
    this.attachmentManager = attachmentManager;
  }

  @GET
  @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
  @Path("/run")
  public Response run(@QueryParam("page") Long pageId)
  {
    ScriptRestResourceModel model = new ScriptRestResourceModel();
    Page page = pageManager.getPage(pageId);
    if (null != page) {
      try {
        runScriptAndAttach(page, model);
      } catch (Exception e) {
        model.setStatus(false);
        model.setError("Error processing file closer or attachment");
        log.error("Something went wrong", e);
      }
    }

    return Response.ok(model).build();
  }

  private void runScriptAndAttach(Page page, ScriptRestResourceModel model) throws IOException
  {
    ExternalScriptRunner runner = new ExternalScriptRunner();
    runner.run(page.getNameForComparison());
    File file = runner.getFile();
    model.setScriptExecutionCode(runner.getCode());
    model.setScriptError(runner.getError());
    model.setScriptOutput(runner.getOutput());

    if (null != file) {
      model.setAttachment(file.getName());
      try {
        Attachment attachment = new Attachment(file.getName(), "text/plain", file.length(), "");
        attachment.setCreationDate(new Date());
        attachment.setLastModificationDate(new Date());
        attachment.setContainer(page);
        page.addAttachment(attachment);
        attachmentManager.saveAttachment(attachment, null, new FileInputStream(file));

      } catch (Exception e) {
        log.error("Attachment went wrong", e);
        model.setStatus(false);
        model.setError("Unable to add attachment");
      } finally {
        runner.close();
      }
    }

    if (runner.getCode() != 0) {
      model.setStatus(false);
      model.setError("Error while executing script");
    } else {
      model.setStatus(true);
    }
  }
}