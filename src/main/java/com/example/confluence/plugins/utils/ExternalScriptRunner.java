package com.example.confluence.plugins.utils;

import com.atlassian.core.util.ClassLoaderUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * DPKJ
 * 22/01/20
 */
public class ExternalScriptRunner implements Closeable
{
  private static final Logger log = LoggerFactory.getLogger(ExternalScriptRunner.class);

  private static final String PYTHON_TARGET = "/usr/bin/python"; //Just adding unix path, this can be modified for OS
  private static final String SCRIPT_PATH = "scripts/script.py";
  private static final String PREFIX = "script-attachment";
  private static final String SUFFIX = ".txt";


  private File file;
  private int code;
  private String output;
  private String error;

  public File getFile()
  {
    return file;
  }

  public int getCode()
  {
    return code;
  }

  public String getOutput()
  {
    return output;
  }

  public String getError()
  {
    return error;
  }

  //Make runner and execute code
  public void run(String content)
  {
    try {
      file = File.createTempFile(PREFIX, SUFFIX);
      file.setWritable(true);
      file.setReadable(true);
      file.deleteOnExit(); //For extra precaution
      String filePath = file.getAbsolutePath();

      //Building process
      ProcessBuilder processBuilder = new ProcessBuilder();
      List<String> command = new ArrayList<>();
      command.add(PYTHON_TARGET);
      Path scriptPath = Paths.get(ClassLoaderUtils.getResource(SCRIPT_PATH, getClass()).toURI());
      command.add(scriptPath.toAbsolutePath().toString()); //Adding script path

      //pass content to write in file
      command.add("-i");
      command.add(content);

      //pass tmp file path
      command.add("-f");
      command.add(filePath);

      //Adding command to process builder
      processBuilder.command(command);

      Process process = processBuilder.start();
      StringBuilder output = new StringBuilder();
      StringBuilder error = new StringBuilder();

      BufferedReader outputReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
      BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

      String line;
      while ((line = outputReader.readLine()) != null) {
        output.append(line).append("\n");
      }

      while ((line = errorReader.readLine()) != null) {
        error.append(line).append("\n");
      }

      this.code = process.waitFor();
      this.output = output.toString();
      this.error = error.toString();

      log.info("Exit code for script: " + this.code);
      log.info("OUTPUT: " + this.output);
      log.info("ERROR: " + this.error);

      if (this.code == 0) {
        log.info("Success!");
      } else {
        log.info("Failure!");
      }

    } catch (Exception e) {
      log.error("Error creating file and writing to it", e);
    }
  }

  private void init() throws IOException
  {
    file = File.createTempFile(PREFIX, SUFFIX);
    file.deleteOnExit();
  }

  @Override
  public void close() throws IOException
  {
    if (null != file) {
      file.delete();
    }
  }

}


