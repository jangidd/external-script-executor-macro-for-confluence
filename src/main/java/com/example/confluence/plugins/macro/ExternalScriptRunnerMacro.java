package com.example.confluence.plugins.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import java.util.Map;

/**
 * DPKJ
 * 22/01/20
 */
public class ExternalScriptRunnerMacro implements Macro
{
  private static final String TEMPLATE = "templates/external-script-macro.vm";

  @Override
  public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException
  {
    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();

    return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
  }

  @Override
  public BodyType getBodyType()
  {
    return BodyType.RICH_TEXT;
  }

  @Override
  public OutputType getOutputType()
  {
    return OutputType.INLINE;
  }
}
